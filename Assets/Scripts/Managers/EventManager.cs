﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void EventHandler(EventArgs agrs);

public class EventManager : Singleton<EventManager> {

	private Queue<Event> events = new Queue<Event> ();
	private Dictionary<string, EventHandler> handlers = new Dictionary<string, EventHandler>();

	private void Awake(){
		StartCoroutine (Execute ());
	}

	public void AddListener(string name, EventHandler listener){
		if (handlers.ContainsKey (name)){
			handlers [name] += listener;
		}else{
			handlers.Add (name, listener);
		}
	}

	public void RemoveListener(string name, EventHandler listener){
		if (handlers.ContainsKey (name)){
			handlers [name] -= listener;
		}
	}

	public void PushEvent(string name, object arg){
		Event e = new Event (name, new EventArgs (arg));

		events.Enqueue (e);
	}

	private IEnumerator Execute(){
		while (true) {

			if (events.Count > 0) {
				Event e = events.Dequeue ();

				if (handlers.ContainsKey (e.eventName) && handlers[e.eventName] != null) {
					handlers [e.eventName] (e.agrs);
				}
			}

			yield return null;
		}
	}
}

public class EventArgs {
	private object value;

	public EventArgs(object v){
		value = v;
	}

	public T GetValue<T>(){
		return (T)value;
	}
}

public class Event {
	public string eventName;
	public EventArgs agrs;

	public Event(string _name, EventArgs _agrs){
		eventName = _name;
		agrs = _agrs;
	}
}
