﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour{

	private static T _instance;
	private static readonly object _lock = new object();

	public static T Instance{
		get{
			lock(_lock){
				if (_instance == null){
					_instance = GameObject.FindObjectOfType <T>();
				}

				if (_instance == null){
					GameObject obj = new GameObject ("Singleton");
					_instance = obj.AddComponent <T> ();
				}

				return _instance;
			}
		}
	}
}
